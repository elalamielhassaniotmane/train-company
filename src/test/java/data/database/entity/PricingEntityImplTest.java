package data.database.entity;

import data.database.entity.intefaces.StationEntity;
import data.database.entity.intefaces.ZoneEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class PricingEntityImplTest {
    @Spy
    private PricingEntityImpl pricingEntity;
    private StationEntity stationEntityA;
    private StationEntity stationEntityB;
    private StationEntity stationEntityC;
    private StationEntity stationEntityD;
    private StationEntity stationEntityE;
    private StationEntity stationEntityF;
    private StationEntity stationEntityG;
    private StationEntity stationEntityH;
    private StationEntity stationEntityI;
    private ZoneEntity zoneEntity1;
    private ZoneEntity zoneEntity2;
    private ZoneEntity zoneEntity3;
    private ZoneEntity zoneEntity4;

    @Before
    public void init() {
        stationEntityA = new StationEntityImpl("A");
        stationEntityB = new StationEntityImpl("B");
        stationEntityC = new StationEntityImpl("C");
        stationEntityD = new StationEntityImpl("D");
        stationEntityE = new StationEntityImpl("E");
        stationEntityF = new StationEntityImpl("F");
        stationEntityG = new StationEntityImpl("G");
        stationEntityH = new StationEntityImpl("H");
        stationEntityI = new StationEntityImpl("I");


        zoneEntity1 = new ZoneEntityImpl(1, Arrays.asList(stationEntityA, stationEntityB));
        zoneEntity2 = new ZoneEntityImpl(2, Arrays.asList(stationEntityC, stationEntityD, stationEntityE));
        zoneEntity3 = new ZoneEntityImpl(3, Arrays.asList(stationEntityC, stationEntityE, stationEntityF));
        zoneEntity4 = new ZoneEntityImpl(4, Arrays.asList(stationEntityF, stationEntityG, stationEntityH, stationEntityI));
    }

    @Test
    public void contain_entry_zone_key_true() {
        Mockito.when(pricingEntity.getEntry())
                .thenReturn(Arrays.asList(zoneEntity1, zoneEntity3));
        assertTrue(pricingEntity.containEntryZoneKey(1));
    }

    @Test
    public void contain_entry_zoneKey_false() {
        Mockito.when(pricingEntity.getEntry())
                .thenReturn(Arrays.asList(zoneEntity1));
        assertFalse(pricingEntity.containEntryZoneKey(2));
    }

    @Test
    public void contain_destination_zonKey_true() {
        Mockito.when(pricingEntity.getDestination())
                .thenReturn(Arrays.asList(zoneEntity1, zoneEntity2));
        assertTrue(pricingEntity.containDestinationZonKey(1));
    }

    @Test
    public void contain_destination_zonKey_false() {
        Mockito.when(pricingEntity.getDestination())
                .thenReturn(Arrays.asList(zoneEntity1, zoneEntity3));
        assertFalse(pricingEntity.containDestinationZonKey(2));
        assertFalse(pricingEntity.containDestinationZonKey(24));
    }

}