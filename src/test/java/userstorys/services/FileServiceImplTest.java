package userstorys.services;

import data.files.input.InputDataEntityImpl;
import data.files.input.TapEntityImpl;
import data.files.input.interfaces.InputDataEntity;
import data.files.output.CustomerSummariesEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import userstorys.model.TripImpl;
import userstorys.model.interfaces.Summary;
import userstorys.model.interfaces.Trip;

import java.time.ZonedDateTime;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class FileServiceImplTest {

    @Mock
    private JsonServiceImpl jsonService;

    private FileServiceImpl fileService;

    @Mock
    private Summary summary;

    @Captor
    private ArgumentCaptor<CustomerSummariesEntity> customerSummariesEntityArgumentCaptor;

    private Trip trip;

    private TapEntityImpl tapEntity = new TapEntityImpl();
    private InputDataEntityImpl inputDataEntity = new InputDataEntityImpl();

    @Before
    public void init() {
        fileService = new FileServiceImpl(jsonService);
        trip = TripImpl.builder()
                .setZoneTo(1)
                .setStationStart("1")
                .setStationEnd("2")
                .setStartedJourneyAt(ZonedDateTime.now())
                .setCostInCents(1)
                .setZoneFrom(2)
                .build();
        tapEntity.setCustomerId(1);
        tapEntity.setStation("A");
        tapEntity.setDate(ZonedDateTime.now());
        inputDataEntity.setTapEntities(Collections.singletonList(tapEntity));
    }


    @Test
    public void export_consumer_summary() {
        Mockito.when(summary.getTotalCostInCents()).thenReturn(1);
        Mockito.when(summary.getCustomerId()).thenReturn(1);
        Mockito.when(summary.getTaps()).thenReturn(null);
        Mockito.when(summary.getTrips()).thenReturn(Collections.singletonList(trip));

        fileService.exportConsumerSummary("", Collections.singletonList(summary));
        Mockito.verify(jsonService).writeOutputFile(Mockito.anyString(),
                customerSummariesEntityArgumentCaptor.capture());
        assertEquals(1, customerSummariesEntityArgumentCaptor.getValue().getCustomerSummaries().size());
        assertEquals(summary.getCustomerId(), customerSummariesEntityArgumentCaptor.getValue().getCustomerSummaries().get(0).getCustomerId());
        assertEquals(summary.getTotalCostInCents(), customerSummariesEntityArgumentCaptor.getValue().getCustomerSummaries().get(0).getTotalCostInCents());

    }

    @Test
    public void read_file() {

        Mockito.when(jsonService.readInputFile(Mockito.anyString(), Mockito.any()))
                .thenReturn(inputDataEntity);
        InputDataEntity inputDataEntity = jsonService.readInputFile("", InputDataEntityImpl.class);
        assertEquals(1, inputDataEntity.getTapEntities().size());
        assertEquals("A", inputDataEntity.getTapEntities().get(0).getStation());
        assertEquals(1, inputDataEntity.getTapEntities().get(0).getCustomerId());
        assertEquals(ZonedDateTime.class, inputDataEntity.getTapEntities().get(0).getDate().getClass());
    }
}