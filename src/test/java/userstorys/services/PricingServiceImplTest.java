package userstorys.services;

import data.database.entity.PricingEntityImpl;
import data.database.entity.StationEntityImpl;
import data.database.entity.ZoneEntityImpl;
import data.database.repositoy.interfaces.PricingRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import userstorys.exception.DataException;
import userstorys.model.interfaces.Pricing;
import userstorys.services.interfaces.PricingService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class PricingServiceImplTest {

    @Mock
    private PricingRepository pricingRepository;

    private PricingService pricingService;

    private PricingEntityImpl pricingEntity1 = new PricingEntityImpl();
    private PricingEntityImpl pricingEntity2 = new PricingEntityImpl();

    private StationEntityImpl stationEntity1 = new StationEntityImpl();
    private StationEntityImpl stationEntity2 = new StationEntityImpl();
    private ZoneEntityImpl zoneEntity1 = new ZoneEntityImpl();
    private ZoneEntityImpl zoneEntity2 = new ZoneEntityImpl();


    @Before
    public void ini() {
        pricingService = new PricingServiceImpl(pricingRepository);

        stationEntity1.setId("A");
        stationEntity2.setId("B");

        zoneEntity1.setZoneId(1);
        zoneEntity1.setStations(Collections.singletonList(stationEntity1));

        zoneEntity2.setZoneId(2);
        zoneEntity2.setStations(Collections.singletonList(stationEntity2));

        pricingEntity1.setCurrency("euro");
        pricingEntity1.setPrice(1);
        pricingEntity1.setDestination(Arrays.asList(zoneEntity1, zoneEntity2));
        pricingEntity1.setEntry(Arrays.asList(zoneEntity1, zoneEntity2));

        pricingEntity2.setCurrency("euro");
        pricingEntity2.setPrice(2);
        pricingEntity2.setDestination(Arrays.asList(zoneEntity1, zoneEntity2));
        pricingEntity2.setEntry(Arrays.asList(zoneEntity1, zoneEntity2));

    }

    @Test
    public void find_by_id() {
        Mockito.when(pricingRepository.findById(1)).thenReturn(pricingEntity1);
        Pricing pricing = pricingService.findById(1);
        assertEquals(pricingEntity1.getCurrency(), pricing.getCurrency());
        assertEquals(pricingEntity1.getPrice(), pricing.getPrice());
        assertEquals(pricingEntity1.getDestination().size(), pricing.getDestination().size());
    }

    @Test(expected = DataException.class)
    public void find_by_id_null() {
        Mockito.when(pricingRepository.findById(1)).thenReturn(null);
        Pricing pricing = pricingService.findById(1);
    }

    @Test
    public void find_by_entry_zone() {
        Mockito.when(pricingRepository.findByEntryZone(Mockito.anyInt()))
                .thenReturn(Collections.singletonList(pricingEntity1));
        List<Pricing> prices = pricingService.findByEntryZone(1);

        assertEquals(pricingEntity1.getCurrency(), prices.get(0).getCurrency());
        assertEquals(pricingEntity1.getPrice(), prices.get(0).getPrice());
        assertEquals(pricingEntity1.getDestination().size(), prices.get(0).getDestination().size());
    }

    @Test
    public void find_by_destination_zone() {
        Mockito.when(pricingRepository.findByDestinationZone(Mockito.anyInt()))
                .thenReturn(Collections.singletonList(pricingEntity1));
        List<Pricing> prices = pricingService.findByDestinationZone(1);

        assertEquals(pricingEntity1.getCurrency(), prices.get(0).getCurrency());
        assertEquals(pricingEntity1.getPrice(), prices.get(0).getPrice());
        assertEquals(pricingEntity1.getDestination().size(), prices.get(0).getDestination().size());
    }

    @Test
    public void find_by_entry_and_destination_zone_id() {
        Mockito.when(pricingRepository.findByEntryAndDestinationZoneId(1, 1))
                .thenReturn(Collections.singletonList(pricingEntity1));
        Mockito.when(pricingRepository.findByEntryAndDestinationZoneId(1, 2))
                .thenReturn(Collections.emptyList());

        List<Pricing> prices = pricingService.findByEntryAndDestinationZoneId(1, 1);
        List<Pricing> pricesEmpty = pricingService.findByEntryAndDestinationZoneId(1, 2);

        assertEquals(0, pricesEmpty.size());
        assertEquals(pricingEntity1.getCurrency(), prices.get(0).getCurrency());
        assertEquals(pricingEntity1.getPrice(), prices.get(0).getPrice());
        assertEquals(pricingEntity1.getDestination().size(), prices.get(0).getDestination().size());
    }

    @Test
    public void getTheSmallestPrice() {
        Mockito.when(pricingRepository.findByEntryAndDestinationZoneId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(Arrays.asList(pricingEntity1, pricingEntity2));

        int price = pricingService.getTheSmallestPrice(1, 1);

        assertEquals(1, price);

    }
}