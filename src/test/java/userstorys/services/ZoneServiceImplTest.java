package userstorys.services;

import data.database.entity.StationEntityImpl;
import data.database.entity.ZoneEntityImpl;
import data.database.repositoy.interfaces.ZoneRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import userstorys.exception.DataException;
import userstorys.model.interfaces.Zone;
import userstorys.services.interfaces.ZoneService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ZoneServiceImplTest {

    @Mock
    private ZoneRepository zoneRepository;

    private ZoneService zoneService;

    private ZoneEntityImpl zoneEntity1 = new ZoneEntityImpl();
    private ZoneEntityImpl zoneEntity2 = new ZoneEntityImpl();

    private StationEntityImpl stationEntity1 = new StationEntityImpl();
    private StationEntityImpl stationEntity2 = new StationEntityImpl();
    private StationEntityImpl stationEntity3 = new StationEntityImpl();
    private StationEntityImpl stationEntity4 = new StationEntityImpl();

    @Before
    public void init() {
        zoneService = new ZoneServiceImpl(zoneRepository);
        stationEntity1.setId("A");
        stationEntity2.setId("B");
        stationEntity3.setId("C");
        stationEntity4.setId("D");

        zoneEntity1.setZoneId(1);
        zoneEntity1.setStations(Arrays.asList(stationEntity1, stationEntity2));

        zoneEntity2.setZoneId(2);
        zoneEntity2.setStations(Arrays.asList(stationEntity3, stationEntity4));
    }

    @Test
    public void findAll() {
        Mockito.when(zoneRepository.findAll()).thenReturn(Arrays.asList(zoneEntity1, zoneEntity2));
        List<Zone> zones = zoneService.findAll();
        assertEquals(2, zones.size());
        assertEquals(zoneEntity1.getZoneId(), zones.get(0).getZoneId());
        assertEquals(zoneEntity2.getZoneId(), zones.get(1).getZoneId());
    }

    @Test
    public void findAll_empty() {
        Mockito.when(zoneRepository.findAll()).thenReturn(Collections.emptyList());
        List<Zone> zones = zoneService.findAll();
        assertEquals(0, zones.size());
    }

    @Test
    public void find_by_id() {
        Mockito.when(zoneRepository.findById(0)).thenReturn(zoneEntity1);
        Zone zone = zoneService.findById(0);
        assertEquals(zoneEntity1.getZoneId(), zone.getZoneId());
        assertEquals(zoneEntity1.getStations().size(), zone.getStation().size());
    }

    @Test(expected = DataException.class)
    public void find_by_id_empty() {
        Mockito.when(zoneRepository.findById(0)).thenReturn(null);
        Zone zone = zoneService.findById(0);
    }

    @Test
    public void find_by_ids() {
        Mockito.when(zoneRepository.findByIds(Arrays.asList(1, 2))).thenReturn(Arrays.asList(zoneEntity1, zoneEntity2));
        List<Zone> zones = zoneService.findByIds(Arrays.asList(1, 2));
        assertEquals(2, zones.size());
        assertEquals(zoneEntity1.getZoneId(), zones.get(0).getZoneId());
        assertEquals(zoneEntity2.getZoneId(), zones.get(1).getZoneId());
    }

    @Test
    public void find_by_station_id() {
        Mockito.when(zoneRepository.findByStationId("A")).thenReturn(Arrays.asList(zoneEntity1, zoneEntity2));
        List<Zone> zones = zoneService.findByStationId("A");
        assertEquals(2, zones.size());
        assertEquals(zoneEntity1.getZoneId(), zones.get(0).getZoneId());
        assertEquals(zoneEntity2.getZoneId(), zones.get(1).getZoneId());
    }
}