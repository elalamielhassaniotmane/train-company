package userstorys.manager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import userstorys.exception.DataException;
import userstorys.model.StationImpl;
import userstorys.model.TapImpl;
import userstorys.model.ZoneImpl;
import userstorys.model.interfaces.Station;
import userstorys.model.interfaces.Summary;
import userstorys.model.interfaces.Tap;
import userstorys.model.interfaces.Zone;
import userstorys.services.interfaces.FileService;
import userstorys.services.interfaces.PricingService;
import userstorys.services.interfaces.ZoneService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class PricingManagerImplTest {

    @Mock
    ZoneService zoneService;

    @Mock
    PricingService pricingService;

    @Mock
    FileService fileService;

    private PricingManagerImpl pricingManager;

    private Tap tap1, tap2;

    private Zone fromZone1, fromZone2, toZone1, toZone2;
    private Station stationA, stationB, stationC, stationD;

    @Captor
    private ArgumentCaptor<List<Summary>> captor;

    @Before
    public void init() {
        pricingManager = new PricingManagerImpl(zoneService, pricingService, fileService);
        tap1 = TapImpl.builder().setCustomerId(1).setStation("A").build();
        tap2 = TapImpl.builder().setCustomerId(1).setStation("D").build();
        stationA = StationImpl.builder().setId("A").build();
        stationB = StationImpl.builder().setId("B").build();
        stationC = StationImpl.builder().setId("C").build();
        stationD = StationImpl.builder().setId("D").build();
        fromZone1 = ZoneImpl.builder()
                .setZoneId(1)
                .setStation(Arrays.asList(stationA, stationB))
                .build();
        toZone1 = ZoneImpl.builder()
                .setZoneId(2)
                .setStation(Collections.singletonList(stationD))
                .build();

        fromZone2 = ZoneImpl.builder()
                .setZoneId(1)
                .setStation(Arrays.asList(stationA, stationB))
                .build();
        toZone2 = ZoneImpl.builder()
                .setZoneId(3)
                .setStation(Arrays.asList(stationC, stationD))
                .build();

    }

    @Test
    public void start_processing() {
        Mockito.when(fileService.readFile("")).thenReturn(Arrays.asList(tap1, tap2));
        Mockito.when(zoneService.findByStationId("A")).thenReturn(Collections.singletonList(fromZone1));
        Mockito.when(zoneService.findByStationId("D")).thenReturn(Collections.singletonList(toZone1));
        Mockito.when(pricingService.getTheSmallestPrice(1, 2)).thenReturn(100);

        pricingManager.startProcessing("", "");
        Mockito.verify(fileService).exportConsumerSummary(Mockito.anyString(), captor.capture());
        assertEquals(1, captor.getValue().size());
        assertEquals(100, captor.getValue().get(0).getTotalCostInCents());
        assertEquals(1, captor.getValue().get(0).getCustomerId());
        assertEquals(1, captor.getValue().get(0).getTrips().size());
    }

    @Test
    public void start_processing_multiple_zone() {
        Mockito.when(fileService.readFile("")).thenReturn(Arrays.asList(tap1, tap2));
        Mockito.when(zoneService.findByStationId("A")).thenReturn(Arrays.asList(fromZone1, fromZone2));
        Mockito.when(zoneService.findByStationId("D")).thenReturn(Arrays.asList(toZone1, toZone2));
        Mockito.when(pricingService.getTheSmallestPrice(1, 2)).thenReturn(400);
        Mockito.when(pricingService.getTheSmallestPrice(1, 3)).thenReturn(300);

        pricingManager.startProcessing("", "");
        Mockito.verify(fileService).exportConsumerSummary(Mockito.anyString(), captor.capture());

        assertEquals(1, captor.getValue().size());
        assertEquals(300, captor.getValue().get(0).getTotalCostInCents());
        assertEquals(1, captor.getValue().get(0).getCustomerId());
        assertEquals(1, captor.getValue().get(0).getTrips().size());
        assertEquals(1, captor.getValue().get(0).getTrips().get(0).getZoneFrom());
        assertEquals(3, captor.getValue().get(0).getTrips().get(0).getZoneTo());
    }

    @Test(expected = DataException.class)
    public void start_processing_exception() {
        Mockito.when(fileService.readFile("")).thenReturn(Collections.singletonList(tap1));
        pricingManager.startProcessing("", "");
    }
}