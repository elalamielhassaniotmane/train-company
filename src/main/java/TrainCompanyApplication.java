import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import data.database.repositoy.PricingRepositoryImpl;
import data.database.repositoy.StationRepositoryImpl;
import data.database.repositoy.ZoneRepositoryImpl;
import data.database.repositoy.interfaces.PricingRepository;
import data.database.repositoy.interfaces.StationRepository;
import data.database.repositoy.interfaces.ZoneRepository;
import userstorys.manager.PricingManagerImpl;
import userstorys.manager.intefaces.PricingManager;
import userstorys.services.FileServiceImpl;
import userstorys.services.JsonServiceImpl;
import userstorys.services.PricingServiceImpl;
import userstorys.services.ZoneServiceImpl;
import userstorys.services.interfaces.FileService;
import userstorys.services.interfaces.JsonService;
import userstorys.services.interfaces.PricingService;
import userstorys.services.interfaces.ZoneService;

import java.util.Scanner;

public class TrainCompanyApplication {
    public static void main(String[] args) {

        // Add Java 8 modules to Jackson, Not asked for it
        ObjectMapper objectMapper = new ObjectMapper()
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule());

        // Init Repository's
        StationRepository stationRepository = new StationRepositoryImpl();
        ZoneRepository zoneRepository = new ZoneRepositoryImpl(stationRepository);
        PricingRepository pricingRepository = new PricingRepositoryImpl(zoneRepository);

        // Init Service's
        JsonService jsonServiceImpl = new JsonServiceImpl(objectMapper);
        ZoneService zoneService = new ZoneServiceImpl(zoneRepository);
        FileService fileService = new FileServiceImpl(jsonServiceImpl);
        PricingService pricingService = new PricingServiceImpl(pricingRepository);

        // Init the main manager of the user Story " Pricing"
        PricingManager pricingManager = new PricingManagerImpl(
                zoneService,
                pricingService,
                fileService);

        try ( Scanner scanner = new Scanner( System.in ) ) {

            System.out.print( "Input Url : " );
            String a = scanner.nextLine();

            System.out.print( "Out put Url : " );
            String b = scanner.nextLine();
            pricingManager.startProcessing(a
                    , b);

        }

    }
}
