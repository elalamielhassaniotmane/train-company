package userstorys.mappers;

import data.files.output.CustomerSummaryEntityImpl;
import data.files.output.interfaces.CustomerSummaryEntity;
import userstorys.exception.DataException;
import userstorys.exception.DataExceptionType;
import userstorys.model.interfaces.Summary;

import java.util.Optional;

public class CustomerSummaryMapper {
    private CustomerSummaryMapper() {
    }

    public static CustomerSummaryEntity from(Summary summary) {
        return Optional.ofNullable(summary)
                .map(summary1 -> CustomerSummaryEntityImpl.builder()
                        .setTotalCostInCents(summary1.getTotalCostInCents())
                        .setCustomerId(summary1.getCustomerId())
                        .setTrips(TripMapper.from(summary1.getTrips()))
                        .build())
                .orElseThrow(() -> new DataException(DataExceptionType.DATA_ERROR));
    }
}
