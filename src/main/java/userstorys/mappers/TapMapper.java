package userstorys.mappers;

import data.files.input.interfaces.TapEntity;
import userstorys.exception.DataException;
import userstorys.exception.DataExceptionType;
import userstorys.model.TapImpl;
import userstorys.model.interfaces.Tap;

import java.util.Optional;

public class TapMapper {
    private TapMapper() {
    }

    public static Tap from(TapEntity tapEntity) {
        return Optional.ofNullable(tapEntity)
                .map(tapEntity1 -> TapImpl.builder()
                        .setCustomerId(tapEntity1.getCustomerId())
                        .setDate(tapEntity1.getDate())
                        .setStation(tapEntity1.getStation())
                        .build())
                .orElseThrow(() -> new DataException(DataExceptionType.DATA_ERROR));

    }
}
