package userstorys.mappers;

import data.database.entity.intefaces.ZoneEntity;
import userstorys.exception.DataException;
import userstorys.exception.DataExceptionType;
import userstorys.model.ZoneImpl;
import userstorys.model.interfaces.Zone;

import java.util.Optional;

public class ZoneMapper {
    private ZoneMapper() {
    }

    public static Zone from(ZoneEntity zoneEntity) {
        return Optional.ofNullable(zoneEntity)
                .map(zoneEntity1 -> ZoneImpl.builder()
                        .setStation(StationMapper.from(zoneEntity1.getStations()))
                        .setZoneId(zoneEntity1.getZoneId())
                        .build())
                .orElseThrow(() -> new DataException(DataExceptionType.DATA_ERROR));
    }
}
