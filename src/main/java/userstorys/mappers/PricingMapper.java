package userstorys.mappers;

import data.database.entity.intefaces.PricingEntity;
import userstorys.exception.DataException;
import userstorys.exception.DataExceptionType;
import userstorys.model.PricingImpl;
import userstorys.model.interfaces.Pricing;

import java.util.Optional;
import java.util.stream.Collectors;

public class PricingMapper {

    private PricingMapper() {
    }

    public static Pricing from(PricingEntity priceEntity) {
        return Optional.ofNullable(priceEntity).map(pricingEntity -> PricingImpl.builder()
                .setCurrency(pricingEntity.getCurrency())
                .setDestination(pricingEntity.getDestination().stream()
                        .map(ZoneMapper::from)
                        .collect(Collectors.toList()))
                .setEntry(pricingEntity.getEntry().stream()
                        .map(ZoneMapper::from)
                        .collect(Collectors.toList()))
                .setPrice(pricingEntity.getPrice())
                .build())
                .orElseThrow(() -> new DataException(DataExceptionType.DATA_ERROR));
    }
}

