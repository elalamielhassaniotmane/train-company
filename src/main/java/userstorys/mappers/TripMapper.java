package userstorys.mappers;

import data.files.output.TripEntityImpl;
import data.files.output.interfaces.TripEntity;
import userstorys.exception.DataException;
import userstorys.exception.DataExceptionType;
import userstorys.model.interfaces.Trip;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TripMapper {
    private TripMapper() {
    }

    public static TripEntity from(Trip trip) {
        return Optional.ofNullable(trip).map(trip1 -> TripEntityImpl.builder()
                .setCostInCents(trip1.getCostInCents())
                .setStartedJourneyAt(trip1.getStartedJourneyAt())
                .setStationEnd(trip1.getStationEnd())
                .setStationStart(trip1.getStationStart())
                .setZoneFrom(trip1.getZoneFrom())
                .setZoneTo(trip1.getZoneTo())
                .build())
                .orElseThrow(() -> new DataException(DataExceptionType.DATA_ERROR));
    }

    public static List<TripEntity> from(List<Trip> trips) {
        return trips.stream()
                .map(TripMapper::from)
                .collect(Collectors.toList());
    }


}
