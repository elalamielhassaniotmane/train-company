package userstorys.mappers;

import data.database.entity.intefaces.StationEntity;
import userstorys.exception.DataException;
import userstorys.exception.DataExceptionType;
import userstorys.model.StationImpl;
import userstorys.model.interfaces.Station;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StationMapper {
    private StationMapper() {
    }

    public static Station from(StationEntity stationEntity) {
        return Optional.ofNullable(stationEntity)
                .map(stationEntity1 -> StationImpl.builder()
                        .setId(stationEntity1.getId())
                        .build())
                .orElseThrow(() -> new DataException(DataExceptionType.DATA_ERROR));
    }

    public static List<Station> from(List<StationEntity> stationEntities) {
        return stationEntities.stream()
                .map(StationMapper::from)
                .collect(Collectors.toList());
    }
}
