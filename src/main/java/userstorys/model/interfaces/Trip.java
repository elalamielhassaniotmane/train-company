package userstorys.model.interfaces;

import java.time.ZonedDateTime;

public interface Trip {
    String getStationStart();

    String getStationEnd();

    ZonedDateTime getStartedJourneyAt();

    int getCostInCents();

    int getZoneFrom();

    int getZoneTo();

}
