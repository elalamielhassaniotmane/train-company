package userstorys.model.interfaces;

import java.time.ZonedDateTime;

public interface Tap {

    ZonedDateTime getDate();

    int getCustomerId();

    String getStation();

}
