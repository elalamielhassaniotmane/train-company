package userstorys.model.interfaces;


import java.util.List;

public interface Pricing {
    List<Zone> getEntry();

    List<Zone> getDestination();

    int getPrice();

    String getCurrency();

}
