package userstorys.model.interfaces;

import java.util.List;

public interface Summary {

    int getCustomerId();

    List<Trip> getTrips();

    List<Tap> getTaps();

    int getTotalCostInCents();
}
