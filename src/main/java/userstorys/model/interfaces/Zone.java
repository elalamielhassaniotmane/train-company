package userstorys.model.interfaces;

import java.util.List;

public interface Zone {
    int getZoneId();

    List<Station> getStation();
}
