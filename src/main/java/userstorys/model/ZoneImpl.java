package userstorys.model;

import userstorys.model.interfaces.Station;
import userstorys.model.interfaces.Zone;

import java.util.List;

public class ZoneImpl implements Zone {
    private int zoneId;
    private List<Station> station;

    private ZoneImpl() {
    }

    public static Builder builder() {
        return new Builder();
    }

    public int getZoneId() {
        return zoneId;
    }

    private void setZoneId(int zoneId) {
        this.zoneId = zoneId;
    }

    public List<Station> getStation() {
        return station;
    }

    private void setStation(List<Station> station) {
        this.station = station;
    }

    public static class Builder {
        private int zoneId;
        private List<Station> station;

        public Builder setZoneId(int zoneId) {
            this.zoneId = zoneId;
            return this;
        }

        public Builder setStation(List<Station> station) {
            this.station = station;
            return this;
        }

        public Zone build() {
            ZoneImpl zone = new ZoneImpl();
            zone.setStation(this.station);
            zone.setZoneId(this.zoneId);
            return zone;
        }
    }
}
