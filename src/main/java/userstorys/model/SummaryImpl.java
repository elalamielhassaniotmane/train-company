package userstorys.model;

import userstorys.model.interfaces.Summary;
import userstorys.model.interfaces.Tap;
import userstorys.model.interfaces.Trip;

import java.util.List;

public class SummaryImpl implements Summary {

    private int customerId;
    private List<Tap> taps;
    private List<Trip> trips;

    private SummaryImpl() {
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(Summary summary) {
        return new Builder(summary);
    }

    @Override
    public int getCustomerId() {
        return customerId;
    }

    private void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Override
    public List<Trip> getTrips() {
        return trips;
    }

    private void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    @Override
    public List<Tap> getTaps() {
        return taps;
    }

    private void setTaps(List<Tap> taps) {
        this.taps = taps;
    }

    @Override
    public int getTotalCostInCents() {
        return this.trips.stream()
                .map(Trip::getCostInCents)
                .mapToInt(Integer::valueOf)
                .sum();
    }

    public static class Builder {
        private int customerId;
        private List<Trip> trips;
        private List<Tap> tap;

        public Builder() {
        }

        public Builder(Summary summary) {
            this.customerId = summary.getCustomerId();
            this.trips = summary.getTrips();
            this.tap = summary.getTaps();
        }

        public Builder setCustomerId(int customerId) {
            this.customerId = customerId;
            return this;
        }

        public Builder setTrips(List<Trip> trips) {
            this.trips = trips;
            return this;
        }

        public Builder setTap(List<Tap> tap) {
            this.tap = tap;
            return this;
        }

        public Summary build() {
            SummaryImpl summary = new SummaryImpl();
            summary.setCustomerId(customerId);
            summary.setTrips(trips);
            summary.setTaps(tap);
            return summary;
        }
    }
}
