package userstorys.model;

import userstorys.model.interfaces.Tap;

import java.time.ZonedDateTime;

public class TapImpl implements Tap {

    private ZonedDateTime date;

    private int customerId;

    private String station;

    private TapImpl() {
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public ZonedDateTime getDate() {
        return date;
    }

    private void setDate(ZonedDateTime date) {
        this.date = date;
    }

    @Override
    public int getCustomerId() {
        return customerId;
    }

    private void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Override
    public String getStation() {
        return station;
    }

    private void setStation(String station) {
        this.station = station;
    }

    public static class Builder {
        private ZonedDateTime date;

        private int customerId;

        private String station;

        public Builder setDate(ZonedDateTime date) {
            this.date = date;
            return this;
        }

        public Builder setCustomerId(int customerId) {
            this.customerId = customerId;
            return this;
        }

        public Builder setStation(String station) {
            this.station = station;
            return this;
        }

        public Tap build() {
            TapImpl tap = new TapImpl();
            tap.setCustomerId(this.customerId);
            tap.setDate(this.date);
            tap.setStation(this.station);
            return tap;
        }
    }
}
