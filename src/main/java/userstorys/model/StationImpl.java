package userstorys.model;

import userstorys.model.interfaces.Station;

public class StationImpl implements Station {
    private String id;

    private StationImpl(String id) {
        this.id = id;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public static class Builder {
        private String id;

        public Builder setId(String id) {
            this.id = id;
            return this;
        }

        public Station build() {
            return new StationImpl(id);
        }
    }
}
