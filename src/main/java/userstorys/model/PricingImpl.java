package userstorys.model;

import userstorys.model.interfaces.Pricing;
import userstorys.model.interfaces.Zone;

import java.util.ArrayList;
import java.util.List;

public class PricingImpl implements Pricing {

    private List<Zone> entry = new ArrayList<>();
    private List<Zone> destination = new ArrayList<>();
    private int price;
    private String currency;

    private PricingImpl() {
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public List<Zone> getEntry() {
        return entry;
    }

    private void setEntry(List<Zone> entry) {
        this.entry = entry;
    }

    @Override
    public List<Zone> getDestination() {
        return destination;
    }

    private void setDestination(List<Zone> destination) {
        this.destination = destination;
    }

    @Override
    public int getPrice() {
        return price;
    }

    private void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    private void setCurrency(String currency) {
        this.currency = currency;
    }

    public static class Builder {

        private List<Zone> entry = new ArrayList<>();
        private List<Zone> destination = new ArrayList<>();
        private int price;
        private String currency;

        public Builder setEntry(List<Zone> entry) {
            this.entry = entry;
            return this;
        }

        public Builder setDestination(List<Zone> destination) {
            this.destination = destination;
            return this;
        }

        public Builder setPrice(int price) {
            this.price = price;
            return this;
        }

        public Builder setCurrency(String currency) {
            this.currency = currency;
            return this;
        }

        public Pricing build() {
            PricingImpl pricing = new PricingImpl();
            pricing.setCurrency(currency);
            pricing.setDestination(destination);
            pricing.setEntry(entry);
            pricing.setPrice(price);
            return pricing;

        }
    }
}


