package userstorys.services;

import data.database.repositoy.interfaces.ZoneRepository;
import userstorys.mappers.ZoneMapper;
import userstorys.model.interfaces.Zone;
import userstorys.services.interfaces.ZoneService;

import java.util.List;
import java.util.stream.Collectors;

public class ZoneServiceImpl implements ZoneService {
    private final ZoneRepository zoneRepository;

    public ZoneServiceImpl(ZoneRepository zoneRepository) {
        this.zoneRepository = zoneRepository;
    }

    @Override
    public List<Zone> findAll() {
        return zoneRepository.findAll().stream()
                .map(ZoneMapper::from)
                .collect(Collectors.toList());
    }

    @Override
    public Zone findById(int id) {
        return ZoneMapper.from(zoneRepository.findById(id));
    }

    @Override
    public List<Zone> findByIds(List<Integer> ids) {
        return zoneRepository.findByIds(ids).stream()
                .map(ZoneMapper::from)
                .collect(Collectors.toList());
    }

    @Override
    public List<Zone> findByStationId(String stationId) {
        return zoneRepository.findByStationId(stationId).stream()
                .map(ZoneMapper::from)
                .collect(Collectors.toList());
    }
}
