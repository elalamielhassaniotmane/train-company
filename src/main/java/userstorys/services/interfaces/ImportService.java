package userstorys.services.interfaces;

import userstorys.model.interfaces.Tap;

import java.util.List;

public interface ImportService {
    List<Tap> readFile(String path);
}
