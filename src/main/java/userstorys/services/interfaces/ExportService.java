package userstorys.services.interfaces;

import userstorys.model.interfaces.Summary;

import java.util.List;

public interface ExportService {
    void exportConsumerSummary(String path, List<Summary> customerSummaries);
}
