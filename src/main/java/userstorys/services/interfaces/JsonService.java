package userstorys.services.interfaces;

public interface JsonService {
    <T> T readInputFile(String path, Class<T> cls);

    void writeOutputFile(String path, Object value);
}
