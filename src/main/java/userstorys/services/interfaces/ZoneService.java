package userstorys.services.interfaces;

import userstorys.model.interfaces.Zone;

import java.util.List;

public interface ZoneService {

    List<Zone> findAll();

    Zone findById(int id);

    List<Zone> findByIds(List<Integer> ids);

    List<Zone> findByStationId(String stationId);
}
