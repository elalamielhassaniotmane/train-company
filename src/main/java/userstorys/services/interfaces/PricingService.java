package userstorys.services.interfaces;

import userstorys.model.interfaces.Pricing;

import java.util.List;

public interface PricingService {
    Pricing findById(int id);

    List<Pricing> findByEntryZone(int zoneId);

    List<Pricing> findByDestinationZone(int zoneId);

    List<Pricing> findByEntryAndDestinationZoneId(int entryId, int destinationId);

    int getTheSmallestPrice(int entryId, int destinationId);
}
