package userstorys.services;

import data.database.repositoy.interfaces.PricingRepository;
import userstorys.exception.DataException;
import userstorys.exception.DataExceptionType;
import userstorys.mappers.PricingMapper;
import userstorys.model.interfaces.Pricing;
import userstorys.services.interfaces.PricingService;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PricingServiceImpl implements PricingService {
    private final PricingRepository pricingRepository;

    public PricingServiceImpl(PricingRepository pricingRepository) {
        this.pricingRepository = pricingRepository;
    }

    @Override
    public Pricing findById(int id) {
        return PricingMapper.from(pricingRepository.findById(id));
    }

    @Override
    public List<Pricing> findByEntryZone(int zoneId) {
        return pricingRepository.findByEntryZone(zoneId).stream()
                .map(PricingMapper::from)
                .collect(Collectors.toList());
    }

    @Override
    public List<Pricing> findByDestinationZone(int zoneId) {
        return pricingRepository.findByDestinationZone(zoneId).stream()
                .map(PricingMapper::from)
                .collect(Collectors.toList());
    }

    @Override
    public List<Pricing> findByEntryAndDestinationZoneId(int entryId, int destinationId) {
        return pricingRepository.findByEntryAndDestinationZoneId(entryId, destinationId)
                .stream()
                .map(PricingMapper::from)
                .collect(Collectors.toList());
    }

    @Override
    public int getTheSmallestPrice(int entryId, int destinationId) {
        return this.findByEntryAndDestinationZoneId(entryId, destinationId)
                .stream().map(Pricing::getPrice)
                .flatMapToInt(IntStream::of)
                .min()
                .orElseThrow(() -> new DataException(DataExceptionType.DATA_ERROR));
    }


}
