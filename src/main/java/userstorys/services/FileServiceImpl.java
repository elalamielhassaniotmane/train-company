package userstorys.services;

import data.files.input.InputDataEntityImpl;
import data.files.input.interfaces.InputDataEntity;
import data.files.output.CustomerSummariesEntity;
import data.files.output.interfaces.CustomerSummaryEntity;
import userstorys.mappers.CustomerSummaryMapper;
import userstorys.mappers.TapMapper;
import userstorys.model.interfaces.Summary;
import userstorys.model.interfaces.Tap;
import userstorys.services.interfaces.FileService;
import userstorys.services.interfaces.JsonService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class FileServiceImpl implements FileService {
    private static final String YYYY_M_MDD_H_HMM_SS = "yyyyMMddHHmmSS";
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(YYYY_M_MDD_H_HMM_SS);
    private final JsonService jsonService;

    public FileServiceImpl(JsonService jsonService) {
        this.jsonService = jsonService;
    }

    @Override
    public void exportConsumerSummary(String path, List<Summary> customerSummaries) {

        List<CustomerSummaryEntity> customerSummaryEntities = customerSummaries.stream()
                .map(CustomerSummaryMapper::from)
                .collect(Collectors.toList());

        CustomerSummariesEntity customerSummariesEntity = CustomerSummariesEntity.builder()
                .setCustomerSummaries(customerSummaryEntities)
                .build();

        jsonService.writeOutputFile(path + "\\output"+ LocalDateTime.now().format(DATE_FORMATTER)
                , customerSummariesEntity);

    }

    public List<Tap> readFile(String path) {
        InputDataEntity inputData = jsonService.readInputFile(path, InputDataEntityImpl.class);
        return inputData.getTapEntities().stream()
                .map(TapMapper::from)
                .collect(Collectors.toList());
    }
}
