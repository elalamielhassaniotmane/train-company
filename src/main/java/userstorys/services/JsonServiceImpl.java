package userstorys.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import userstorys.exception.InputException;
import userstorys.exception.InputExceptiontype;
import userstorys.services.interfaces.JsonService;

import java.io.File;
import java.io.IOException;

public class JsonServiceImpl implements JsonService {
    private final ObjectMapper mapper;

    public JsonServiceImpl(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public <T> T readInputFile(String path, Class<T> cls) {
        try {
            return mapper.readValue(new File(path), cls);
        } catch (IOException e) {
            throw new InputException(InputExceptiontype.ERROR_READING_INPUT_FILE);
        }
    }

    @Override
    public void writeOutputFile(String path, Object value) {
        try {
            mapper.writeValue(new File(path), value);
        } catch (IOException e) {
            throw new InputException(InputExceptiontype.ERROR_WRITTING_TO_FILE);
        }
    }


}
