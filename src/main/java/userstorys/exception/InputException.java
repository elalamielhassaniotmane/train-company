package userstorys.exception;

public class InputException extends RuntimeException {
    private final InputExceptiontype inputExceptiontype;

    public InputException(InputExceptiontype inputExceptiontype) {
        this.inputExceptiontype = inputExceptiontype;
    }

    @Override
    public String getMessage() {
        switch (inputExceptiontype) {
            case ERROR_READING_INPUT_FILE:
                return "An Error had occurred when reading input file";
            case ERROR_WRITTING_TO_FILE:
                return "An Error  had occurred when writing output file ";
            default:
                return "An unknown error had occurred";
        }
    }
}
