package userstorys.exception;

public class DataException extends RuntimeException {
    private final DataExceptionType dataExceptionType;

    public DataException(DataExceptionType dataExceptionType) {
        this.dataExceptionType = dataExceptionType;
    }

    @Override
    public String getMessage() {
        switch (dataExceptionType) {
            case DATA_ERROR:
                return "Error mapping data";
            default:
                return "Unknown Error";
        }
    }
}
