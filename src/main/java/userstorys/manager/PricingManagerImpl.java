package userstorys.manager;

import userstorys.exception.DataException;
import userstorys.exception.DataExceptionType;
import userstorys.manager.intefaces.PricingManager;
import userstorys.model.SummaryImpl;
import userstorys.model.TripImpl;
import userstorys.model.interfaces.Summary;
import userstorys.model.interfaces.Tap;
import userstorys.model.interfaces.Trip;
import userstorys.model.interfaces.Zone;
import userstorys.services.interfaces.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class PricingManagerImpl implements PricingManager {

    private final ZoneService zoneService;
    private final PricingService pricingService;
    private final ExportService exportService;
    private final ImportService importService;

    public PricingManagerImpl(ZoneService zoneService,
                              PricingService pricingService,
                              FileService fileService) {
        this.zoneService = zoneService;
        this.pricingService = pricingService;
        this.importService = fileService;
        this.exportService = fileService;
    }

    @Override
    public void startProcessing(String inputPath, String outputPath) {

        List<Tap> taps = importService.readFile(inputPath);

        // GroupingBy is used because it's preserve the order in the list
        Map<Integer, List<Tap>> groupedTaps = taps.stream()
                .collect(Collectors.groupingBy(Tap::getCustomerId));

        List<Summary> customerSummaries = groupedTaps.keySet().stream()
                .map(userId -> SummaryImpl.builder()
                        .setCustomerId(userId)
                        .setTap(groupedTaps.get(userId))
                        .build())
                .map(customerSummary -> SummaryImpl.builder(customerSummary)
                        .setTrips(this.getTrips(customerSummary.getTaps()))
                        .build())
                .collect(Collectors.toList());

        exportService.exportConsumerSummary(outputPath, customerSummaries);

    }

    private List<Trip> getTrips(List<Tap> taps) {
        List<Trip> trips = new ArrayList<>();
        if (taps.size() % 2 != 0) {
            throw new DataException(DataExceptionType.DATA_ERROR);
        }
        for (int i = 0; i < taps.size() / 2; i++) {
            Trip trip = buildTrip(taps.get(i * 2), taps.get(i * 2 + 1));
            trips.add(trip);
        }
        return trips;
    }

    private Trip buildTrip(Tap from, Tap to) {
        List<Zone> fromZones = zoneService.findByStationId(from.getStation());
        List<Zone> toZones = zoneService.findByStationId(to.getStation());
        int minPrice = 0;
        Optional<Zone> startZone = Optional.empty();
        Optional<Zone> destinationZone = Optional.empty();

        for (int i = 0; i < fromZones.size(); i++) {
            for (int y = 0; y < toZones.size(); y++) {
                int price = pricingService.getTheSmallestPrice(fromZones.get(i).getZoneId(),
                        toZones.get(y).getZoneId());
                // the first Loop we set the zone and the prices
                if (i == 0 && y == 0) {
                    minPrice = price;
                    startZone = Optional.of(fromZones.get(i));
                    destinationZone = Optional.of(toZones.get(y));
                }

                if (minPrice >= price) {
                    startZone = Optional.of(fromZones.get(i));
                    destinationZone = Optional.of(toZones.get(y));
                    minPrice = price;
                }
            }
        }
        return TripImpl.builder()
                .setCostInCents(minPrice)
                .setStartedJourneyAt(from.getDate())
                .setStationEnd(to.getStation())
                .setStationStart(from.getStation())
                .setZoneFrom(startZone.orElseThrow(() -> new DataException(DataExceptionType.DATA_ERROR)).getZoneId())
                .setZoneTo(destinationZone.orElseThrow(() -> new DataException(DataExceptionType.DATA_ERROR)).getZoneId())
                .build();
    }
}
