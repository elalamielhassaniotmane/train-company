package data.files.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import data.files.input.interfaces.InputDataEntity;

import java.util.ArrayList;
import java.util.List;

public class InputDataEntityImpl implements InputDataEntity {
    @JsonProperty("taps")
    private List<TapEntityImpl> tapEntities = new ArrayList<>();

    public List<TapEntityImpl> getTapEntities() {
        return tapEntities;
    }

    public void setTapEntities(List<TapEntityImpl> tapEntities) {
        this.tapEntities = tapEntities;
    }
}
