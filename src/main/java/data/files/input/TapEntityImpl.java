package data.files.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import data.files.input.interfaces.TapEntity;

import java.time.ZonedDateTime;

public class TapEntityImpl implements TapEntity {
    @JsonProperty("unixTimestamp")
    private ZonedDateTime date;

    private int customerId;

    private String station;

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }
}
