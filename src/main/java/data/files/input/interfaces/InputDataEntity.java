package data.files.input.interfaces;

import data.files.input.TapEntityImpl;

import java.util.List;

public interface InputDataEntity {
    List<TapEntityImpl> getTapEntities();
}
