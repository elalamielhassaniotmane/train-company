package data.files.input.interfaces;

import java.time.ZonedDateTime;

public interface TapEntity {
    ZonedDateTime getDate();

    int getCustomerId();

    String getStation();
}
