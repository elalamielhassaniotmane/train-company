package data.files.output;

import data.files.output.interfaces.TripEntity;

import java.time.ZonedDateTime;

public class TripEntityImpl implements TripEntity {
    private String stationStart;
    private String stationEnd;
    private ZonedDateTime startedJourneyAt;
    private int costInCents;
    private int zoneFrom;
    private int zoneTo;

    public static Builder builder() {
        return new Builder();
    }

    public String getStationStart() {
        return stationStart;
    }

    public void setStationStart(String stationStart) {
        this.stationStart = stationStart;
    }

    public String getStationEnd() {
        return stationEnd;
    }

    public void setStationEnd(String stationEnd) {
        this.stationEnd = stationEnd;
    }

    public ZonedDateTime getStartedJourneyAt() {
        return startedJourneyAt;
    }

    public void setStartedJourneyAt(ZonedDateTime startedJourneyAt) {
        this.startedJourneyAt = startedJourneyAt;
    }

    public int getCostInCents() {
        return costInCents;
    }

    public void setCostInCents(int costInCents) {
        this.costInCents = costInCents;
    }

    public int getZoneFrom() {
        return zoneFrom;
    }

    public void setZoneFrom(int zoneFrom) {
        this.zoneFrom = zoneFrom;
    }

    public int getZoneTo() {
        return zoneTo;
    }

    public void setZoneTo(int zoneTo) {
        this.zoneTo = zoneTo;
    }

    public static class Builder {
        private String stationStart;
        private String stationEnd;
        private ZonedDateTime startedJourneyAt;
        private int costInCents;
        private int zoneFrom;
        private int zoneTo;

        public Builder setStationStart(String stationStart) {
            this.stationStart = stationStart;
            return this;
        }

        public Builder setStationEnd(String stationEnd) {
            this.stationEnd = stationEnd;
            return this;
        }

        public Builder setStartedJourneyAt(ZonedDateTime startedJourneyAt) {
            this.startedJourneyAt = startedJourneyAt;
            return this;
        }

        public Builder setCostInCents(int costInCents) {
            this.costInCents = costInCents;
            return this;
        }

        public Builder setZoneFrom(int zoneFrom) {
            this.zoneFrom = zoneFrom;
            return this;
        }

        public Builder setZoneTo(int zoneTo) {
            this.zoneTo = zoneTo;
            return this;
        }

        public TripEntity build() {
            TripEntityImpl trip = new TripEntityImpl();
            trip.setCostInCents(costInCents);
            trip.setStartedJourneyAt(startedJourneyAt);
            trip.setStationEnd(stationEnd);
            trip.setStationStart(stationStart);
            trip.setZoneFrom(zoneFrom);
            trip.setZoneTo(zoneTo);
            return trip;
        }
    }
}
