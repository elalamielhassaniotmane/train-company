package data.files.output;

import data.files.output.interfaces.CustomerSummaryEntity;
import data.files.output.interfaces.TripEntity;

import java.util.List;

public class CustomerSummaryEntityImpl implements CustomerSummaryEntity {

    private int customerId;
    private int totalCostInCents;
    private List<TripEntity> trips;

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public int getCustomerId() {
        return customerId;
    }

    @Override
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Override
    public int getTotalCostInCents() {
        return totalCostInCents;
    }

    @Override
    public void setTotalCostInCents(int totalCostInCents) {
        this.totalCostInCents = totalCostInCents;
    }

    @Override
    public List<TripEntity> getTrips() {
        return trips;
    }

    @Override
    public void setTrips(List<TripEntity> trips) {
        this.trips = trips;
    }

    public static class Builder {
        private int customerId;
        private int totalCostInCents;
        private List<TripEntity> trips;

        public Builder setCustomerId(int customerId) {
            this.customerId = customerId;
            return this;
        }

        public Builder setTotalCostInCents(int totalCostInCents) {
            this.totalCostInCents = totalCostInCents;
            return this;
        }

        public Builder setTrips(List<TripEntity> trips) {
            this.trips = trips;
            return this;
        }

        public CustomerSummaryEntity build() {
            CustomerSummaryEntity outputCustomerSummary = new CustomerSummaryEntityImpl();
            outputCustomerSummary.setCustomerId(customerId);
            outputCustomerSummary.setTotalCostInCents(totalCostInCents);
            outputCustomerSummary.setTrips(trips);
            return outputCustomerSummary;
        }
    }
}
