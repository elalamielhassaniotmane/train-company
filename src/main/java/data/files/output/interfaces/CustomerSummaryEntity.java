package data.files.output.interfaces;

import java.util.List;

public interface CustomerSummaryEntity {

    int getCustomerId();

    void setCustomerId(int customerId);

    int getTotalCostInCents();

    void setTotalCostInCents(int totalCostInCents);

    List<TripEntity> getTrips();

    void setTrips(List<TripEntity> trips);
}
