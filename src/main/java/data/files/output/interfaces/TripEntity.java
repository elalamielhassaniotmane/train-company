package data.files.output.interfaces;

import java.time.ZonedDateTime;

public interface TripEntity {
    String getStationStart();

    String getStationEnd();

    ZonedDateTime getStartedJourneyAt();

    int getCostInCents();

    int getZoneFrom();

    int getZoneTo();
}
