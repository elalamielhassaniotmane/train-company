package data.files.output;

import data.files.output.interfaces.CustomerSummaryEntity;

import java.util.List;

public class CustomerSummariesEntity {

    private List<CustomerSummaryEntity> customerSummaries;

    public static Builder builder() {
        return new Builder();
    }

    public List<CustomerSummaryEntity> getCustomerSummaries() {
        return customerSummaries;
    }

    private void setCustomerSummaries(List<CustomerSummaryEntity> customerSummaries) {
        this.customerSummaries = customerSummaries;
    }

    public static class Builder {

        private List<CustomerSummaryEntity> customerSummaries;

        public Builder setCustomerSummaries(List<CustomerSummaryEntity> customerSummaries) {
            this.customerSummaries = customerSummaries;
            return this;
        }

        public CustomerSummariesEntity build() {
            CustomerSummariesEntity customerSummariesEntity = new CustomerSummariesEntity();
            customerSummariesEntity.setCustomerSummaries(customerSummaries);
            return customerSummariesEntity;
        }
    }


}
