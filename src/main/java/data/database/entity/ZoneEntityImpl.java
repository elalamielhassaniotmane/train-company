package data.database.entity;

import data.database.entity.intefaces.StationEntity;
import data.database.entity.intefaces.ZoneEntity;

import java.util.List;

public class ZoneEntityImpl implements ZoneEntity {
    private int zoneId;
    private List<StationEntity> stations;

    public ZoneEntityImpl() {
    }

    public ZoneEntityImpl(int zoneId, List<StationEntity> stations) {
        this.zoneId = zoneId;
        this.stations = stations;
    }

    @Override
    public int getZoneId() {
        return zoneId;
    }

    public void setZoneId(int zoneId) {
        this.zoneId = zoneId;
    }

    @Override
    public List<StationEntity> getStations() {
        return stations;
    }

    public void setStations(List<StationEntity> stations) {
        this.stations = stations;
    }

    @Override
    public boolean containsStation(String stationId) {
        return this.getStations().stream()
                .anyMatch(stationEntity -> stationEntity.getId().equals(stationId));
    }

}
