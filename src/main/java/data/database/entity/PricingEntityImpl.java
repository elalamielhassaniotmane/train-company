package data.database.entity;

import data.database.entity.intefaces.PricingEntity;
import data.database.entity.intefaces.ZoneEntity;

import java.util.ArrayList;
import java.util.List;

public class PricingEntityImpl implements PricingEntity {
    private int id;
    private List<ZoneEntity> entry = new ArrayList<>();
    private List<ZoneEntity> destination = new ArrayList<>();
    private int price;
    private String currency;

    public PricingEntityImpl() {
    }

    public PricingEntityImpl(int id) {
        this.id = id;
    }

    public PricingEntityImpl(int id, List<ZoneEntity> entry,
                             List<ZoneEntity> destination,
                             int price,
                             String currency) {
        this.id = id;
        this.entry = entry;
        this.destination = destination;
        this.price = price;
        this.currency = currency;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public List<ZoneEntity> getEntry() {
        return entry;
    }

    public void setEntry(List<ZoneEntity> entry) {
        this.entry = entry;
    }

    @Override
    public List<ZoneEntity> getDestination() {
        return destination;
    }

    public void setDestination(List<ZoneEntity> destination) {
        this.destination = destination;
    }

    @Override
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public boolean containEntryZoneKey(int entryId) {
        return this.getEntry().stream()
                .anyMatch(zoneEntity -> zoneEntity.getZoneId() == entryId);
    }

    @Override
    public boolean containDestinationZonKey(int destinationId) {
        return this.getDestination().stream()
                .anyMatch(zoneEntity -> zoneEntity.getZoneId() == destinationId);
    }

}
