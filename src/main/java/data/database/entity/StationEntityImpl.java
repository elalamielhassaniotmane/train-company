package data.database.entity;

import data.database.entity.intefaces.StationEntity;

import java.util.Objects;

public class StationEntityImpl implements StationEntity {

    private String id;

    public StationEntityImpl() {
    }

    public StationEntityImpl(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StationEntityImpl that = (StationEntityImpl) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "StationEntity{" +
                "id='" + id + '\'' +
                '}';
    }
}
