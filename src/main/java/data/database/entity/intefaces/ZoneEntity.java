package data.database.entity.intefaces;

import java.util.List;

public interface ZoneEntity {
    int getZoneId();

    List<StationEntity> getStations();

    boolean containsStation(String stationId);
}
