package data.database.entity.intefaces;

import java.util.List;

public interface PricingEntity {
    int getId();

    List<ZoneEntity> getEntry();

    List<ZoneEntity> getDestination();

    int getPrice();

    String getCurrency();

    boolean containEntryZoneKey(int entryId);

    boolean containDestinationZonKey(int destinationId);
}
