package data.database.repositoy;

import data.database.entity.StationEntityImpl;
import data.database.entity.intefaces.StationEntity;
import data.database.repositoy.interfaces.StationRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StationRepositoryImpl implements StationRepository {
    private Map<String, StationEntityImpl> stations;
    private List<String> ids = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I");

    public StationRepositoryImpl() {
        this.stations = ids.stream()
                .collect(Collectors.toMap(s -> s, StationEntityImpl::new));
    }

    @Override
    public StationEntityImpl findById(String id) {
        return stations.get(id);
    }


    @Override
    public List<StationEntity> findAll() {
        return new ArrayList<>(stations.values());
    }

    @Override
    public List<StationEntity> findByIds(List<String> ids) {
        return ids.stream()
                .map(id -> stations.get(id))
                .collect(Collectors.toList());
    }
}
