package data.database.repositoy.interfaces;

import data.database.entity.intefaces.PricingEntity;

import java.util.List;

public interface PricingRepository {
    PricingEntity findById(int id);

    List<PricingEntity> findByEntryZone(int zoneId);

    List<PricingEntity> findByDestinationZone(int zoneId);

    List<PricingEntity> findByEntryAndDestinationZoneId(int entryId, int destinationId);
}
