package data.database.repositoy.interfaces;

import data.database.entity.intefaces.StationEntity;

import java.util.List;

public interface StationRepository {
    List<StationEntity> findByIds(List<String> ids);

    List<StationEntity> findAll();

    StationEntity findById(String id);
}
