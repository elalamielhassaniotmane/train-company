package data.database.repositoy.interfaces;

import data.database.entity.intefaces.ZoneEntity;

import java.util.List;

public interface ZoneRepository {

    List<ZoneEntity> findAll();

    ZoneEntity findById(int id);

    List<ZoneEntity> findByIds(List<Integer> ids);

    List<ZoneEntity> findByStationId(String stationId);
}
