package data.database.repositoy;

import data.database.entity.ZoneEntityImpl;
import data.database.entity.intefaces.ZoneEntity;
import data.database.repositoy.interfaces.StationRepository;
import data.database.repositoy.interfaces.ZoneRepository;

import java.util.*;
import java.util.stream.Collectors;


public class ZoneRepositoryImpl implements ZoneRepository {
    private Map<Integer, ZoneEntityImpl> zones = new HashMap<>();

    public ZoneRepositoryImpl(StationRepository stationRepositoryImpl) {
        zones.put(1, new ZoneEntityImpl(1, stationRepositoryImpl.findByIds(Arrays.asList("A", "B"))));
        zones.put(2, new ZoneEntityImpl(2, stationRepositoryImpl.findByIds(Arrays.asList("C", "D", "E"))));
        zones.put(3, new ZoneEntityImpl(3, stationRepositoryImpl.findByIds(Arrays.asList("C", "E", "F"))));
        zones.put(4, new ZoneEntityImpl(4, stationRepositoryImpl.findByIds(Arrays.asList("F", "G", "H", "I"))));
    }

    @Override
    public List<ZoneEntity> findAll() {
        return new ArrayList<>(zones.values());
    }

    @Override
    public ZoneEntity findById(int id) {
        return zones.get(id);
    }

    @Override
    public List<ZoneEntity> findByIds(List<Integer> ids) {
        return ids.stream()
                .map(id -> zones.get(id))
                .collect(Collectors.toList());
    }

    @Override
    public List<ZoneEntity> findByStationId(String stationId) {
        return zones.values().stream()
                .filter(zoneEntity -> zoneEntity.containsStation(stationId))
                .collect(Collectors.toList());
    }
}
