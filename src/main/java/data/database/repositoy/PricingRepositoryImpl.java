package data.database.repositoy;

import data.database.entity.PricingEntityImpl;
import data.database.entity.intefaces.PricingEntity;
import data.database.repositoy.interfaces.PricingRepository;
import data.database.repositoy.interfaces.ZoneRepository;

import java.util.*;
import java.util.stream.Collectors;

public class PricingRepositoryImpl implements PricingRepository {
    private Map<Integer, PricingEntityImpl> pricingMap = new HashMap<>();

    public PricingRepositoryImpl(ZoneRepository zoneRepository) {
        pricingMap.put(1,
                new PricingEntityImpl(1,
                        zoneRepository.findByIds(Arrays.asList(1, 2)),
                        zoneRepository.findByIds(Arrays.asList(1, 2)),
                        240,
                        "euro"
                ));
        pricingMap.put(2,
                new PricingEntityImpl(2,
                        zoneRepository.findByIds(Arrays.asList(3, 4)),
                        zoneRepository.findByIds(Arrays.asList(3, 4)),
                        200,
                        "euro"
                ));

        pricingMap.put(3,
                new PricingEntityImpl(3,
                        zoneRepository.findByIds(Collections.singletonList(3)),
                        zoneRepository.findByIds(Arrays.asList(1, 2)),
                        280,
                        "euro"
                ));

        pricingMap.put(4,
                new PricingEntityImpl(4,
                        zoneRepository.findByIds(Collections.singletonList(4)),
                        zoneRepository.findByIds(Arrays.asList(1, 2)),
                        300,
                        "euro"
                ));
        pricingMap.put(5,
                new PricingEntityImpl(5,
                        zoneRepository.findByIds(Arrays.asList(1, 2)),
                        zoneRepository.findByIds(Collections.singletonList(3)),
                        280,
                        "euro"
                ));
        pricingMap.put(6,
                new PricingEntityImpl(6,
                        zoneRepository.findByIds(Arrays.asList(1, 2)),
                        zoneRepository.findByIds(Collections.singletonList(4)),
                        300,
                        "euro"
                ));
    }

    @Override
    public PricingEntityImpl findById(int id) {
        return pricingMap.get(id);
    }

    @Override
    public List<PricingEntity> findByEntryZone(int zoneId) {
        return pricingMap.values()
                .stream()
                .filter(pricingEntityImpl -> pricingEntityImpl.containEntryZoneKey(zoneId))
                .collect(Collectors.toList());
    }

    @Override
    public List<PricingEntity> findByDestinationZone(int zoneId) {
        return pricingMap.values()
                .stream()
                .filter(pricingEntityImpl -> pricingEntityImpl.containDestinationZonKey(zoneId))
                .collect(Collectors.toList());
    }

    @Override
    public List<PricingEntity> findByEntryAndDestinationZoneId(int entryId, int destinationId) {
        return pricingMap.values()
                .stream()
                .filter(pricingEntityImpl -> pricingEntityImpl.containDestinationZonKey(destinationId))
                .filter(pricingEntityImpl -> pricingEntityImpl.containEntryZoneKey(entryId))
                .collect(Collectors.toList());
    }
}
